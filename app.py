﻿#import cx_Oracle
from signal import signal, SIGPIPE, SIG_DFL
signal(SIGPIPE, SIG_DFL) 
import os
import datetime
os.environ["NLS_LANG"] = "Russian.AL32UTF8"
import config
import cx_Oracle
from flask_wtf import Form
from wtforms import StringField

def num(s):
    try:
        return int(s)
    except ValueError:
        return s

orcl = cx_Oracle.connect(config.ORACLE_CONNECT)
curs = orcl.cursor()
clnt = curs.var (cx_Oracle.STRING)

def writelog(s):
    with open('log.txt','a') as f:
        f.write('\n' + str(datetime.datetime.today())+ '  ' +s)

def writetrash(s):
    with open('trash.txt','a') as f:
        f.write('\n' + str(datetime.datetime.today())+ '  ' +s)

from flask import Flask ,redirect,url_for
from flask import request
from flask import render_template
app = Flask(__name__,instance_relative_config=True)
app.config.from_object('config')


@app.route('/', methods=['GET','POST'])
def index():
            if request.method == 'POST':
                    if (type(num(request.form['account'])) is int ):
                            account = request.form['account'].encode('utf-8')
                            curs.callfunc("GET_ACC_INFO4BOT", clnt,[account])
                            if clnt.getvalue() == "Неверный лицевой счет":
                                    info = str(clnt.getvalue())
                                    writelog(account + ' ' +info)
                            else:
                                    info = clnt.getvalue().split('\n')
                                    writelog(account + ' ' + ' '.join(info))
                            try:
                                    return render_template("index.html",result=info, account=account)
                            except:
                                    writelog("Detected remote disconnect")
                    else :
			try:
				writetrash(request.form['account'].encode('utf-8'))
				return render_template("index.html",result="Не распознан", account="Лицевой счет")
			except:
				writelog("Detected remote disconnect")
            try:
                    return render_template("index.html",account="Лицевой счет" )
            except :
                    writelog("Detected remote disconnect")


@app.route('/redirect/', methods=['GET','POST'])
def redirect():
            if request.method == 'POST':
                    if (type(num(request.form['account'])) is int ):
                            account = request.form['account'].encode('utf-8')
                            curs.callfunc("GET_ACC_INFO4BOT", clnt,[account])
                            if clnt.getvalue() == "Неверный лицевой счет":
                                    info = str(clnt.getvalue())
                                    writelog(account + ' ' +info)
                            else:
                                    info = clnt.getvalue().split('\n')
                                    writelog(account + ' ' + ' '.join(info) + ' Redirected')
                            try:
                                    return render_template("redirect.html",result=info, account=account)
                            except :
                                     writelog("Detected remote disconnect")
                    else :
                            try:
				    writetrash(request.form['account'].encode('utf-8'))
                                    return render_template("redirect.html",result="Не распознан", account="Лицевой счет")
                            except :
                                     writelog("Detected remote disconnect")

            try:
                    return render_template("redirect.html",account="Лицевой счет" )
            except :
                    writelog("Detected remote disconnect")


while True:
	try:
		app.run(debug=False,port=8000, host='0.0.0.0')
	except:
		pass
